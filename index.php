<?php
//Scan directory for files
$arr_json_files = scandir('./fixtures');
$arr_config_data = array();
$arr_all_config_data = array();

foreach ($arr_json_files as $key => $json_file) {

	// Chack it is valid file and a not sub directory
	if($json_file != '.' && $json_file !='..' && is_file('fixtures/'.$json_file)) {

		// Chack file is valid json file or not
		if(is_valid_json('fixtures/'.$json_file)) {

			$arr_all_config_data[] = $arr_config_data = json_decode(file_get_contents('fixtures/'.$json_file), true);
		}
	}
}

/**
 * It Chaecks gien file is valid json file or not
 * if it is valid file then return true else false
 * 
 * @param Staring $file
 * @return bool
 */

function is_valid_json($json_file)
{
	json_decode(file_get_contents($json_file), true);
	return (json_last_error() == JSON_ERROR_NONE) ? true : false;
}


echo "<pre>";
// all valid config files data
print_r($arr_all_config_data);

// last valid config file data. It was overridden previous version of valid config file data
print_r($arr_config_data);
print_r($arr_config_data['database']['host']);